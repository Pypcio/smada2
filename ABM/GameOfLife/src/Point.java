import java.util.ArrayList;
import java.util.Random;

public class Point {
    private ArrayList<Point> neighbors;
    private int currentState;
    private int nextState;
    private int numStates = 6;
    private boolean isRain = false;
    private Rule rule = Rule.CITIES;

    public Point() {
        currentState = 0;
        nextState = 0;
        neighbors = new ArrayList<>();
    }

    public void clicked() {
        currentState = (++currentState) % numStates;
    }

    public int getState() {
        return currentState;
    }

    public void setState(int s) {
        currentState = s;
    }

    public void calculateNewState() {
        if (isRain) {
            if (currentState > 0) {
                nextState = currentState - 1;
            } else {
                if (!neighbors.isEmpty() && neighbors.get(0).currentState > 0) {
                    nextState = 6;
                }
            }
        } else {
            int activeNeighbors = countActiveNeighbors();
            StateStatus nextStateStatus;
            if (isAlive()) {
                nextStateStatus = rule.nextStateForAliveState(activeNeighbors);
            } else {
                nextStateStatus = rule.nextStateForDeadState(activeNeighbors);
            }
            if (nextStateStatus == StateStatus.DEAD) {
                nextState = 0;
            } else {
                nextState = 1;
            }
        }

    }

    public void changeState() {
        currentState = nextState;
    }

    public void addNeighbor(Point nei) {
        neighbors.add(nei);
    }

    private int countActiveNeighbors() {
        int counter = 0;
        for (Point neighbor : neighbors) {
            if (neighbor.isAlive()) {
                counter++;
            }
        }
        return counter;
    }

    private boolean isAlive() {
        return currentState > 0;
    }

    public void drop() {
        int rand = new Random().nextInt(100);
        if (rand <= 5) {
            nextState = 6;
        }
    }
}
