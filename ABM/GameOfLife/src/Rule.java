import java.util.Arrays;
import java.util.List;

public enum Rule {
    STANDARD(Arrays.asList(2, 3), 3),
    CITIES(Arrays.asList(2, 3, 4, 5), Arrays.asList(4, 5, 6, 7, 8)),
    CORAL(Arrays.asList(4, 5, 6, 7, 8), 3);

    private final List<Integer> stayAlive;
    private final List<Integer> becomesAlive;

    Rule(List<Integer> stayAlive, List<Integer> becomesAlive) {
        this.stayAlive = stayAlive;
        this.becomesAlive = becomesAlive;
    }

    Rule(List<Integer> stayAlive, int becomesAlive) {
        this.stayAlive = stayAlive;
        this.becomesAlive = Arrays.asList(becomesAlive);
    }

    public StateStatus nextStateForAliveState(int activeNeighbours) {
        if (stayAlive.contains(activeNeighbours))
            return StateStatus.ALIVE;
        return StateStatus.DEAD;
    }

    public StateStatus nextStateForDeadState(int activeNeighbours) {
        if (becomesAlive.contains(activeNeighbours))
            return StateStatus.ALIVE;
        return StateStatus.DEAD;
    }
}
