package kis.sspd.jade.farm;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;

@SuppressWarnings("serial")
public class Wolf extends Agent {
    protected int age = 0;
    static int adultAge = 2;

    protected void setup() {
        System.out.println("Wolf " + getLocalName() + " is getting closer to the farm.");
        addBehaviour(new Live());
        addBehaviour(new GetHungry());
    }


    private class Live extends CyclicBehaviour {
        public void action() {
            block(1000);
            age += 1;
            if (age == adultAge) {
                System.out.println("Wold is adult");
            }
        }
    }

    private class GetHungry extends CyclicBehaviour {
        public void action() {
            block(1000);
            ACLMessage aclMessage = new ACLMessage(ACLMessage.REQUEST);
            aclMessage.addReceiver(new AID(Breeder.BREEDER_NAME, AID.ISLOCALNAME));
            aclMessage.setContent("randomRabbitName");
            send(aclMessage);
            System.out.println("Wolf is hungry");

            ACLMessage message2 = myAgent.blockingReceive();
            if (message2 != null) {
                String rabbitName2 = message2.getContent();

                ACLMessage aclMessage3 = new ACLMessage(ACLMessage.REQUEST);
                aclMessage3.addReceiver(new AID(rabbitName2, AID.ISLOCALNAME));
                aclMessage3.setContent(getName());
                send(aclMessage3);
                System.out.println("Wolf is going to eat" + rabbitName2);
            }
        }
    }

    @SuppressWarnings("unused")
    private class GetOld extends OneShotBehaviour {
        public void action() {
            //TODO
        }
    }

    @SuppressWarnings("unused")
    private class EatBunny extends OneShotBehaviour {
        public void action() {
            //TODO

        }
    }


    protected void takeDown() {
        System.out.println("Wolf " + getLocalName() + " is daying.");
    }
}
