package kis.sspd.jade.farm;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;


@SuppressWarnings("serial")
public class RabbitMale extends Agent {
	protected int age = 0;
	static int lifetime = 10;
	static int adultAge = 2;
	
	protected void setup(){
		System.out.println("Male " + getLocalName() + " appeared on the farm.");
		addBehaviour(new Live());
		addBehaviour(new Behave());
	}
	
	
	
	private class Live extends CyclicBehaviour {
		public void action() {
			block(1000);
			age +=1 ;
			if (age == lifetime){
				ACLMessage aclMessage = new ACLMessage(ACLMessage.INFORM);
				aclMessage.addReceiver(new AID(Breeder.BREEDER_NAME, AID.ISLOCALNAME));
				aclMessage.setContent(getName());
				send(aclMessage);
				System.out.println("Rabbit male is too old");
			} else if (age == adultAge){
				System.out.println("Rabbit male is becoming adult");
			}

		}
	}
	
	private class Behave extends CyclicBehaviour {
		public void action() {
			ACLMessage messageFromWolf = myAgent.blockingReceive(1000);
			if (messageFromWolf != null) {
				ACLMessage aclMessage = new ACLMessage(ACLMessage.INFORM);
				aclMessage.addReceiver(new AID(Breeder.BREEDER_NAME, AID.ISLOCALNAME));
				aclMessage.setContent(getName());
				send(aclMessage);
				System.out.println("Wolf is eating me");

				String wolfName = messageFromWolf.getContent();

				ACLMessage messageToWolf = new ACLMessage(ACLMessage.INFORM);
				messageToWolf.addReceiver(new AID(wolfName, AID.ISLOCALNAME));
				messageToWolf.setContent(getName());
				send(messageToWolf);
			}
		}
	}
	
	@SuppressWarnings("unused")
	private class GetOlder extends OneShotBehaviour {
		public void action() {
			//TODO
		}
	}
	
	@SuppressWarnings("unused")
	private class TryToHaveChildren extends OneShotBehaviour {
		public void action() {
			//TODO
		}
	}
	
	@SuppressWarnings("unused")
	private class Court extends OneShotBehaviour {
		public void action() {
			//TODO
		}
	}
	
	protected void takeDown(){
		System.out.println("Male " + getLocalName() + " is dying.");
	}
}
