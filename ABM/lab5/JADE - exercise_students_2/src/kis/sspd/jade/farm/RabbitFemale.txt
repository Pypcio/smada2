package kis.sspd.jade.farm;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;

@SuppressWarnings("serial")
public class RabbitFemale extends Agent {
	protected int age = 0;
	static int lifetime = 10;
	static int adultAge = 2;
	
	protected void setup(){
		System.out.println("Female " + getLocalName() + " appeared on the farm.");
		addBehaviour(new Live());
		addBehaviour(new Listen());
	}
	
	
	private class Live extends CyclicBehaviour {
		public void action() {
			block(1000);
			age +=1 ;
			if (age == lifetime){
				ACLMessage aclMessage = new ACLMessage(ACLMessage.INFORM);
				aclMessage.addReceiver(new AID(Breeder.BREEDER_NAME, AID.ISLOCALNAME));
				aclMessage.setContent(getName());
				send(aclMessage);
				System.out.println("Rabbit female is too old");
			} else if (age == adultAge){
				System.out.println("Rabbit female is becoming adult");
			}
		}
	}
	
	private class Listen extends CyclicBehaviour {
		public void action() {
			//TODO
			ACLMessage messageFromWolf = myAgent.blockingReceive(1000);
			if (messageFromWolf != null) {
				ACLMessage aclMessage = new ACLMessage(ACLMessage.INFORM);
				aclMessage.addReceiver(new AID(Breeder.BREEDER_NAME, AID.ISLOCALNAME));
				aclMessage.setContent(getName());
				send(aclMessage);
				System.out.println("Wolf is eating me");

				String wolfName = messageFromWolf.getContent();

				ACLMessage messageToWolf = new ACLMessage(ACLMessage.INFORM);
				messageToWolf.addReceiver(new AID(wolfName, AID.ISLOCALNAME));
				messageToWolf.setContent(getName());
				send(messageToWolf);
			}
		}
	}
	
	@SuppressWarnings("unused")
	private class BreedNewRabbits extends OneShotBehaviour {
		public void action() {
			//TODO
		}
	}
	
	protected void takeDown(){
		System.out.println("Famale " + getLocalName() + " is dying.");
	}
}
