import java.util.Random;

public class Point {

    public Point nNeighbor;
    public Point wNeighbor;
    public Point eNeighbor;
    public Point sNeighbor;
    public float nVel;
    public float eVel;
    public float wVel;
    public float sVel;
    public float pressure;
    public static Integer[] types = {0, 1, 2};
    int type = 0;
    int sinInput = 0;

    public Point() {
        clear();
    }

    public void clicked() {
        pressure = 1;
    }

    public void clear() {
        // TODO: clear velocity and pressure
        nVel = 0;
        sVel = 0;
        wVel = 0;
        eVel = 0;
        pressure = 0;
    }

    public void updateVelocity() {
        // TODO: velocity update
        if (type == 0){
            nVel -= nNeighbor.pressure - this.pressure;
            sVel -= sNeighbor.pressure - this.pressure;
            wVel -= wNeighbor.pressure - this.pressure;
            eVel -= eNeighbor.pressure - this.pressure;
        }
    }

    public void updatePresure() {
        // TODO: pressure update
        if (type == 0) {
            pressure = (float) (pressure - (0.5 * (nVel + sVel
                    + eVel + wVel)));
        } else if (type == 2){
            double radians = Math.toRadians(sinInput);
            pressure = (float) (Math.sin(radians));
        }
        if (sinInput >= 360){
            sinInput = 0;
        } else {
            sinInput += 20;
        }
    }

    public float getPressure() {
        return pressure;
    }
}