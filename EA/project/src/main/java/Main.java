import javafx.application.Application;
import javafx.geometry.Side;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.stage.Stage;
import service.LssOssAlhorithm;
import service.Sequence;

import java.util.Comparator;
import java.util.List;

/**
 * Main class of the project
 */
public class Main extends Application {

    private static final int SIZE = 101;

    private static final int MINUTES_LIMIT = 10;

    private static final int TARGET_LIMIT = 578	;

    private static final int WIDTH = 800;

    private static final int HEIGHT = 480;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("Chart");


        LssOssAlhorithm lssOssAlhorithm = new LssOssAlhorithm();
        List<Sequence> result = lssOssAlhorithm.calculate(SIZE, MINUTES_LIMIT, TARGET_LIMIT);
        LineChart chart = createChart(result);
//        System.out.println(result);
        Sequence bestSequence = result.stream()
                .min(Comparator.comparing(Sequence::value))
                .get();
        System.out.println("==== BEST SEQUENCE with smallest value ====");
        System.out.println(bestSequence);
        Scene scene = new Scene(chart, WIDTH, HEIGHT);
        scene.getStylesheets().add("style.css");
        primaryStage.setScene(scene);

        primaryStage.show();
    }

    private LineChart createChart(List<Sequence> sequences) {
        final NumberAxis xAxis = new NumberAxis();
        final NumberAxis yAxis = new NumberAxis();
        xAxis.setLabel("Iteration");
        yAxis.setLabel("Energy");
        final LineChart<Number, Number> lineChart = new LineChart<>(xAxis, yAxis);
        lineChart.setCreateSymbols(false);
        lineChart.setLegendVisible(false);

        XYChart.Series currentSeries = new XYChart.Series();
        currentSeries.setName("current energy");
        int smallestEnergy = Integer.MAX_VALUE;
        XYChart.Series lowestResult = new XYChart.Series();
        lowestResult.setName("best result");
        for (int i = 0; i < sequences.size(); i++) {
            if (smallestEnergy > sequences.get(i).value()) {
                smallestEnergy = sequences.get(i).value();
            }
            currentSeries.getData().add(new XYChart.Data(i, sequences.get(i).value()));
            lowestResult.getData().add(new XYChart.Data(i, smallestEnergy));
        }
        lineChart.getData().add(currentSeries);
        lineChart.getData().add(lowestResult);
        lineChart.setLegendVisible(true);
        lineChart.setLegendSide(Side.BOTTOM);
        lineChart.setCreateSymbols(false);
        return lineChart;
    }
}
