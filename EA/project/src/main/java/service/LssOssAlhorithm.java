package service;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.IntStream;

public class LssOssAlhorithm {

    private Set<Sequence> closePivots = new LinkedHashSet<>();

    public List<Sequence> calculate(int size, int minutesLimit, int targetValue) {
        LocalDateTime start = LocalDateTime.now();
        Sequence currSequence = Sequence.createRandomUniqueInstance(size, closePivots);
        while (!isValueBetterThanTarget(targetValue, currSequence) && !isTimeLimitExceeded(minutesLimit, start)){
            for (int k = 0; k < 4 * (size + 1); k++) {
                closePivots.add(currSequence);
                Optional<Sequence> bestNeighbour = findTheBestUniqueNeighbour(size, currSequence);
                if (bestNeighbour.isPresent()) {
                    currSequence = bestNeighbour.get();
                } else {
                    break;
                }
            }
            currSequence = Sequence.createRandomUniqueInstance(size, closePivots);
        }
        return new ArrayList<>(closePivots);
    }

    private boolean isValueBetterThanTarget(int targetValue, Sequence currSequence) {
        return currSequence.value() <= targetValue;
    }

    private boolean isTimeLimitExceeded(int timeLimit, LocalDateTime start) {
        return Duration.between(start, LocalDateTime.now()).toMinutes() >= timeLimit;
    }

    //    TODO - consider. What if neighbours had worse values ? IMO - nothing, will be worse then previous one result. Ask on project meeting.
    private Optional<Sequence> findTheBestUniqueNeighbour(int size, Sequence currSeq) {
        return IntStream.range(0, (size + 1) / 2)
                .mapToObj(currSeq::flip)
                .filter(seq -> !closePivots.contains(seq))
                .min(Comparator.comparing(Sequence::value));
    }
}

// chech guava cache - urochomić 10 minut z size 101 oraz 151
// wykres z srednich