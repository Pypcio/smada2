package service;

import java.util.*;

public class Sequence implements Comparable<Sequence> {
    private final List<Integer> elements;
    private Integer value;

    public Sequence(List<Integer> elements) {
        this.elements = elements;
        this.value = calculateEnergyOfSequence();
    }

    public Integer value() {
        return value;
    }

    public Sequence flip(int idx) {
        ArrayList<Integer> copiedElements = new ArrayList<>(elements);
        copiedElements.set(idx, elements.get(idx) * (-1));
        return new Sequence(copiedElements);
    }

    private int calculateEnergyOfSequence() {
        List<Integer> stepResults = new ArrayList<>();
        int size = elements.size();
        for (int k = 1; k < size; k++) {
            List<Integer> values = new ArrayList<>();
            for (int i = 0; i + k < size; i++) {
                values.add(elements.get(i) * elements.get(i + k));
            }
            stepResults.add(values.stream().mapToInt(a -> a).sum());
        }
        return stepResults.stream().mapToInt(a -> a * a).sum();
    }

    public static Sequence createRandomUniqueInstance(int seqSize, Set<Sequence> closePivots) {
        List<Integer> result = new ArrayList<>();
        Random random = new Random();
        do {
            for (int i = 0; i < seqSize; i++) {
                int val = random.nextBoolean() ? 1 : -1;
                result.add(val);
            }
        } while (closePivots.contains(result));
        return new Sequence(result);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Sequence sequence = (Sequence) o;
        return Objects.equals(elements, sequence.elements);
    }

    @Override
    public int hashCode() {
        return Objects.hash(elements);
    }

    @Override
    public int compareTo(Sequence o) {
        return Comparator.comparing(Sequence::value).compare(this, o);
    }

    @Override
    public String toString() {
        return "sequence: " + this.elements + "\nvalue: " + this.value;
    }
}
