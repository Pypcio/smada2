package service;

import java.util.List;

public interface Function {

    int apply(List<Integer> elements);
}
