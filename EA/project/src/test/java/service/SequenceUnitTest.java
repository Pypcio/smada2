package service;

import org.junit.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import static org.assertj.core.api.BDDAssertions.then;

public class SequenceUnitTest {
    @Test
    public void shouldCalculate() {
        // given
        List<Integer> elements = Arrays.asList(1, -1, 1, -1);
        // when
        Sequence sequence = new Sequence(elements);
        // then
        then(sequence.value())
                .isEqualTo(14);
    }

    @Test
    public void shouldCreateUniqueSequence() {
        //given
        Sequence sequenceAlreadyUsed = new Sequence(Arrays.asList(-1));
        //when
        Sequence result = Sequence.createRandomUniqueInstance(1, new HashSet<>(Arrays.asList(sequenceAlreadyUsed)));

        //then
        then(result.equals(new Sequence(Arrays.asList(1)))).isTrue();
    }
}