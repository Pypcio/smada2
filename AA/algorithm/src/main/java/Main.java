import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultWeightedEdge;
import services.DijstraPath;
import services.DijstraService;
import services.GraphService;

import java.io.File;

public class Main {

    public static void main(String args[]) {

        ClassLoader classLoader = Main.class.getClassLoader();
        File file = new File(classLoader.getResource("nodes.txt").getFile());

        Graph<Integer, DefaultWeightedEdge> graph = GraphService.createGraph(file);
        GraphService.fillGraph(graph, file);
        System.out.println("filled graph");
        DijstraService dijstraService = new DijstraService(graph);
        DijstraPath result = dijstraService.findPath(1,20);
        System.out.println("total cost: " + result.getTotalCost() + "\npath: " + result.getNodes());
    }
}
