package services;

import java.util.ArrayList;
import java.util.List;

public class DijstraPath {

    private List<Integer> nodes = new ArrayList<>();
    private int totalCost;

    public DijstraPath(List<Integer> nodes, int totalCost) {
        this.nodes = nodes;
        this.totalCost = totalCost;
    }

    public List<Integer> getNodes() {
        return nodes;
    }

    public void setNodes(List<Integer> nodes) {
        this.nodes = nodes;
    }

    public int getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(int totalCost) {
        this.totalCost = totalCost;
    }
}
