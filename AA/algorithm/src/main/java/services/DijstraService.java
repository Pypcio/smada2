package services;

import com.google.common.collect.Lists;
import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultWeightedEdge;

import java.util.*;

public class DijstraService {

    private final Graph<Integer, DefaultWeightedEdge> graph;

    public DijstraService(Graph<Integer, DefaultWeightedEdge> graph) {
        this.graph = graph;
    }

    public DijstraPath findPath(Integer source, Integer destination) {
        Map<Integer, Integer> pathCosts = initPathCost(source);
        Map<Integer, Integer> previousVertex = initPathMap(source);
        Set<Integer> leftVertexes = new HashSet<>(graph.vertexSet());
        Integer currentVertex = source;
        while (!leftVertexes.isEmpty()) {
            leftVertexes.remove(currentVertex);
            updateMaps(previousVertex, pathCosts, currentVertex);
            Optional<Integer> nextVertex = findSmallestVertex(leftVertexes, pathCosts);
            if (nextVertex.isPresent()) {
                currentVertex = nextVertex.get();
            } else {
                break;
            }
        }
        return new DijstraPath(getVertexesInOrder(source, destination, previousVertex), pathCosts.get(destination));
    }

    private List<Integer> getVertexesInOrder(Integer source, Integer destination, Map<Integer, Integer> vertexesInPath) {
        List<Integer> vertexes = new ArrayList<>();
        Integer lastVertex = destination;
        while (!lastVertex.equals(source)) {
            vertexes.add(lastVertex);
            lastVertex = vertexesInPath.get(lastVertex);
        }
        vertexes.add(source);
        return Lists.reverse(vertexes);
    }

    private Optional<Integer> findSmallestVertex(Set<Integer> leftVertexes, Map<Integer, Integer> pathCosts) {
        return pathCosts.entrySet().stream().filter(x -> leftVertexes.contains(x.getKey()))
                .min(Comparator.comparing(Map.Entry::getValue))
                .map(Map.Entry::getKey);
    }

    private Map<Integer, Integer> initPathCost(int source) {
        Map<Integer, Integer> costMap = new HashMap<>();
        for (Integer i : graph.vertexSet()) {
            costMap.put(i, Integer.MAX_VALUE);
        }
        costMap.put(source, 0);
        return costMap;
    }

    private Map<Integer, Integer> initPathMap(int source) {
        Map<Integer, Integer> costMap = new HashMap<>();
        for (Integer i : graph.vertexSet()) {
            costMap.put(i, Integer.MAX_VALUE);
        }
        costMap.put(source, source);
        return costMap;
    }

    private void updateMaps(Map<Integer, Integer> previousVertex, Map<Integer, Integer> pathCosts, Integer vertex) {
        Set<DefaultWeightedEdge> edges = graph.outgoingEdgesOf(vertex);
        Integer sourceCost = pathCosts.get(vertex);
        for (DefaultWeightedEdge edge : edges) {
            int edgeCost = (int) graph.getEdgeWeight(edge);
            Integer edgeDestination = graph.getEdgeTarget(edge);
            Integer previousCost = previousVertex.get(edgeDestination);
            if (sourceCost + edgeCost < previousCost) {
                pathCosts.put(edgeDestination, edgeCost + sourceCost);
                previousVertex.put(edgeDestination, vertex);
            }
        }
    }

}
