package services;

import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.WeightedPseudograph;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class GraphService {

    private static final String DISTIBUTOR = ";";

    private static final int BREAK_VALUES = 2;

    public static Graph<Integer, DefaultWeightedEdge> createGraph(File file) {
        Graph<Integer, DefaultWeightedEdge> graph = new WeightedPseudograph<>(DefaultWeightedEdge.class);
        Scanner scanner = null;
        try {
            scanner = new Scanner(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Set<Integer> nodes = new HashSet<>();
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            nodes.add(Integer.valueOf(line.substring(0, line.indexOf(DISTIBUTOR))));
            nodes.add(Integer.valueOf(line.substring(line.indexOf(DISTIBUTOR) + BREAK_VALUES, line.lastIndexOf(DISTIBUTOR))));
        }
        for (Integer node : nodes) {
            graph.addVertex(node);
        }
        return graph;
    }

    public static void fillGraph(Graph<Integer, DefaultWeightedEdge> graph, File file) {
        Scanner scanner = null;
        try {
            scanner = new Scanner(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            Integer source = Integer.valueOf(line.substring(0, line.indexOf(DISTIBUTOR)));
            Integer destination = Integer.valueOf(line.substring(line.indexOf(DISTIBUTOR) + BREAK_VALUES, line.lastIndexOf(DISTIBUTOR)));
            Integer weight = Integer.valueOf(line.substring(line.lastIndexOf(DISTIBUTOR) + BREAK_VALUES));
            DefaultWeightedEdge edge = graph.addEdge(source, destination);
            graph.setEdgeWeight(edge, weight);
        }
    }
}
