import model.Edge;
import model.Face;
import model.Point;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import service.GeometricService;

import java.math.BigDecimal;
import java.math.BigInteger;

public class GeometricTest {

    private GeometricService geometricService;

    @Before
    public void setup() {
        geometricService = new GeometricService();
    }

    @Test
    public void shouldCountDistanceBetweenPoints() {
//        given
        Point p0 = new Point(0, 0, 0);
        Point p1 = new Point(4, 3, 0);
//        when
        double distance = geometricService.dist(p0, p1);
        BigInteger intVal = BigDecimal.valueOf(distance).toBigInteger();
//        then
        Assert.assertEquals(5, intVal.intValue());

    }

    /**
     * Tests if correctly count angle between two edges
     *
     * @throws Exception
     */
    @Test
    public void shouldCountAngle() throws Exception {
//        given
        Point p0 = new Point(0, 0, 0);
        Point p1 = new Point(4, 3, 0);
        Point p2 = new Point(2, 5, 0);
//        when
        double val = geometricService.angle(new Edge(p0, p1), new Edge(p0, p2));
        BigInteger intVal = BigDecimal.valueOf(val).toBigInteger();
//        then
        Assert.assertEquals(31, intVal.intValue());
    }

    /**
     * Tests if correctly count distance between edge and point
     */
    @Test
    public void shouldCalculateDistancePointToEdge() {
//        given
        Point p0 = new Point(1, 3, 0);
        Point p1 = new Point(-1, 3, 0);
        Point p2 = new Point(10, 3, 0);
        Point pointE0 = new Point(0, 0, 0);
        Point pointE1 = new Point(2, 0, 0);
        Edge edge = new Edge(pointE0, pointE1);
//        when
        double val0 = geometricService.dist(p0, edge);
        BigInteger val0Int = BigDecimal.valueOf(val0).toBigInteger();
        double val1 = geometricService.dist(p1, edge);
        double val2 = geometricService.dist(p2, edge);
//        than
        Assert.assertTrue(val0Int.intValue() == 3);
        Assert.assertTrue(val1 > 3);
        Assert.assertTrue(val2 > 3);

    }

    /**
     * Tests if correctly count distance between edge and point
     */
    @Test
    public void shouldCalculateDistanceEdgeToEdge() {
//        given
        Point p0 = new Point(-3, 0, 0);
        Point p1 = new Point(1, 0, 0);
        Edge edge0 = new Edge(p0, p1);

        Point p2 = new Point(4, 4, 0);
        Point p3 = new Point(6, 4, 0);
        Edge edge1 = new Edge(p2, p3);

        Point p4 = new Point(-1, 4, 0);
        Point p5 = new Point(6, 4, 0);
        Edge edge2 = new Edge(p4, p5);

        Point p6 = new Point(-1, -2, 0);
        Point p7 = new Point(1, 2, 0);
        Edge edge3 = new Edge(p6, p7);

//        when
//        double val1 = geometricService.dist(edge0, edge1);
//        BigInteger val1Int = BigDecimal.valueOf(val1).toBigInteger();
//
//        double val2 = geometricService.dist(edge0, edge2);
//        BigInteger val2Int = BigDecimal.valueOf(val2).toBigInteger();
//
//        double val3 = geometricService.dist(edge1, edge2);
//        BigInteger val3Int = BigDecimal.valueOf(val3).toBigInteger();

        double val4 = geometricService.dist(edge0, edge3);
        BigInteger val4Int = BigDecimal.valueOf(val4).toBigInteger();

        //        than
//        Assert.assertEquals(5, val1Int.intValue());
//        Assert.assertEquals(4, val2Int.intValue());
//        Assert.assertEquals(0, val3Int.intValue());
        Assert.assertEquals(0, val4Int.intValue());

    }

    @Test
    public void shouldCorrectlyCalculateFaceToFace(){
        Face face0 = new Face(new Point(1.0, 1.0, 0.0), new Point(2.0, 2.0, 0.0), new Point(0.0, 2.0, 0.0));
        Face face1 = new Face(new Point(1.0, 1.0, 5.0), new Point(2.0, 2.0, 5.0), new Point(0.0, 2.0, 5.0));
        double distance = geometricService.dist(face0, face1);
        BigDecimal bcDistance = new BigDecimal(distance);
        Assert.assertEquals(BigDecimal.valueOf(5.0), bcDistance.setScale(1, BigDecimal.ROUND_HALF_DOWN));
    }

    @Test
    public void shouldCorrectlyCalculateFaceToPoint(){
        Face face0 = new Face(new Point(1.0, 1.0, 0.0), new Point(2.0, 2.0, 0.0), new Point(0.0, 2.0, 0.0));
        Point point = new Point(0, 1, 0);
        double distance = geometricService.dist(face0, point);
        BigDecimal bcDistance = new BigDecimal(distance);
        Assert.assertEquals(BigDecimal.valueOf(0.7), bcDistance.setScale(1, BigDecimal.ROUND_HALF_DOWN));
    }

    /**
     * Calculates result for 2a exercise. Result should be around 0.13
     */
    @Test
    public void exe2a() {
        Face face0 = new Face(new Point(1.0, 0.0, 0.0), new Point(0.0, 1.0, 0.0), new Point(0.0, 0.0, 0.0));
        Face face1 = new Face(new Point(100.0, 0.0, 0.13), new Point(0.0, 100.0, 0.13), new Point(-100.0, -100.0, 0.13));
        double distance = geometricService.dist(face0, face1);
        BigDecimal bcDistance = new BigDecimal(distance);
        Assert.assertEquals(BigDecimal.valueOf(0.13), bcDistance.setScale(2, BigDecimal.ROUND_HALF_DOWN));
    }

}
