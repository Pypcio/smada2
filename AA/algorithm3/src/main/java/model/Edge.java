package model;

import service.GeometricService;

public class Edge {

    /**
     * First point
     */
    private Point v1;

    /**
     * Second point
     */
    private Point v2;

    /**
     * Creates edge
     * @param v1
     * @param v2
     */
    public Edge(Point v1, Point v2) {
        this.v1 = v1;
        this.v2 = v2;
    }

    /**
     * Gets v1
     * @return v1
     */
    public Point getV1() {
        return v1;
    }

    /**
     * Sets v1
     * @param v1
     */
    public void setV1(Point v1) {
        this.v1 = v1;
    }

    /**
     * Gets v2
     * @return v2
     */
    public Point getV2() {
        return v2;
    }

    /**
     * Sets v2
     * @param v2
     */
    public void setV2(Point v2) {
        this.v2 = v2;
    }



//    public double angle(Edge e1, Edge e2, GeometricService geoService) {
//        Point a;
//        Point b;
//        if (e1.getV1().equals(e2.getV1())) {
//            a = e1.getV2().getDistance(e1.getV1());
//            b = e2.getV2().getDistance(e2.getV1());
//        } else {
//            a = e1.getV2().getDistance(e1.getV1());
//            b = e2.getV2().getDistance(e2.getV2());
//        }
//        Point shared = new Point(0, 0, 0);
//        double scalar = a.getX() * b.getX() + a.getY() * b.getY() + a.getZ() * b.getZ();
//        double distA = geoService.dist(a, shared);
//        double distB = dist(b, shared);
//        double cos = scalar / (distA * distB);
//        double angle = Math.acos(cos);
//        return Math.toDegrees(angle);
//    }


}
