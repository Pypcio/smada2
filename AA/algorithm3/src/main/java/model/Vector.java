package model;

public class Vector {
    private double x;
    private double y;
    private double z;

    public Vector(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }


    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getZ() {
        return z;
    }

    public double length() {
        return Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2) + Math.pow(z, 2));
    }

    public Vector vectorMultiply(Vector vec) {
        return new Vector(
                this.y * vec.z - this.z * vec.y,
                this.z * vec.x - this.x * vec.z,
                this.x * vec.y - this.y * vec.x
        );
    }

    public double scalarMultiply(Vector vec) {
        return this.x * vec.x + this.y * vec.y + this.z * vec.z;
    }


    public Vector multiply(double multiplier) {
        return new Vector(x * multiplier, y * multiplier, z * multiplier);
    }
}
