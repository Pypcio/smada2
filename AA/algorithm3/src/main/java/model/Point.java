package model;

public class Point {

    /**
     * z variable
     */
    private double x;

    /**
     * y variable
     */
    private double y;

    /**
     * z variable
     */
    private double z;

    /**
     * Creates point
     * @param x
     * @param y
     * @param z
     */
    public Point(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    /**
     * Gets x
     * @return x
     */
    public double getX() {
        return x;
    }

    /**
     * Sets x
     * @param x
     */
    public void setX(double x) {
        this.x = x;
    }

    /**
     * Sets y
     * @return y
     */
    public double getY() {
        return y;
    }

    /**
     * Sets y
     * @param y
     */
    public void setY(double y) {
        this.y = y;
    }

    /**
     * Gets z
     * @return
     */
    public double getZ() {
        return z;
    }

    /**
     * Sets z
     * @param z
     */
    public void setZ(double z) {
        this.z = z;
    }

    public Point getDistance(Point p){
        double x = this.x-p.getX();
        double y = this.y-p.getY();
        double z = this.z-p.getZ();
        return new Point(x,y,z);
    }

    public boolean equals(Point point){
        return x==point.getX() && y == point.getY() && z == point.getZ();
    }

    public Vector vectorToOtherPoint(Point point) {
        return new Vector(point.x - this.x, point.y - this.y, point.z - this.z);
    }

    public Vector toVectorFromZeroZeroZero(){
        return new Vector(x,y,z);
    }

    public Point add(Vector vector) {
        return new Point(x + vector.getX() , y + vector.getY(), z + vector.getZ());
    }
}
