package model;

import java.util.ArrayList;
import java.util.List;

public class Face {

    /**
     * First point
     */
    private Point v1;

    /**
     * Second point
     */
    private Point v2;

    /**
     * Third point
     */
    private Point v3;

    /**
     * Creates face
     * @param v1
     * @param v2
     * @param v3
     */
    public Face(Point v1, Point v2, Point v3) {
        this.v1 = v1;
        this.v2 = v2;
        this.v3 = v3;
    }

    /**
     * Gets v1
     * @return v1
     */
    public Point getV1() {
        return v1;
    }

    /**
     * Sets v1
     * @param v1
     */
    public void setV1(Point v1) {
        this.v1 = v1;
    }

    /**
     * Gets v2
     * @return v2
     */
    public Point getV2() {
        return v2;
    }

    /**
     * Sets v2
     * @param v2
     */
    public void setV2(Point v2) {
        this.v2 = v2;
    }

    /**
     * Gets v3
     * @return v3
     */
    public Point getV3() {
        return v3;
    }

    /**
     * Sets v3
     * @param v3
     */
    public void setV3(Point v3) {
        this.v3 = v3;
    }

    public List<Edge> getEdges(){
        List<Edge> edges = new ArrayList<>();
        edges.add(new Edge(v1,v2));
        edges.add(new Edge(v1,v3));
        edges.add(new Edge(v2,v3));
        return edges;
    }

    public double calculateArea() {
        return normalToFace().length() / 2;
    }

    public Vector normalToFace() {
        Vector u = v3.vectorToOtherPoint(v2);
        Vector v = v3.vectorToOtherPoint(v1);
        return u.vectorMultiply(v);
    }
}
