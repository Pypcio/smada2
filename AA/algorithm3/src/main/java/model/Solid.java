package model;

import java.util.List;

public class Solid {

    /**
     * List of faces
     */
    List<Face> faces;

    /**
     * size
     */
    int size;

    /**
     * Creates solid
     * @param faces
     * @param size
     */
    public Solid(List<Face> faces, int size) {
        this.faces = faces;
        this.size = size;
    }

    /**
     * Gets faces list
     * @return face list
     */
    public List<Face> getFaces() {
        return faces;
    }

    /**
     * Sets faces list
     * @param faces
     */
    public void setFaces(List<Face> faces) {
        this.faces = faces;
    }

    /**
     * Gets size
     * @return size
     */
    public int getSize() {
        return size;
    }

    /**
     * Sets size
     * @param size
     */
    public void setSize(int size) {
        this.size = size;
    }

    /**
     * Adds to list
     * @param face
     */
    public void addToList(Face face){
        this.faces.add(face);
    }
}
