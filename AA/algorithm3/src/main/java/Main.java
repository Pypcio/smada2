import model.Face;
import model.Point;
import model.Solid;
import service.DataReader;
import service.GeometricService;

import java.io.File;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        DataReader dataReader = new DataReader();
        ClassLoader classLoader = Main.class.getClassLoader();
        File file = new File(classLoader.getResource("solid_data.txt").getFile());
        List<Solid> solids = dataReader.createSolids(file);
        GeometricService geometricService = new GeometricService();

        Face face0 = new Face(new Point(1.0, 0.0, 0.0), new Point(0.0, 1.0, 0.0), new Point(0.0, 0.0, 0.0));
        Face face1 = new Face(new Point(100.0, 0.0, 0.13), new Point(0.0, 100.0, 0.13), new Point(-100.0, -100.0, 0.13));
        double exe2a = geometricService.dist(face0, face1);
        System.out.println("2a: " + exe2a);

        double exe2b = geometricService.dist(solids.get(0), solids.get(1));
        System.out.println("2b: " + exe2b);
    }
}
