package service;

import model.Face;
import model.Point;
import model.Solid;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class DataReader {

    private static final String SREDNIK = ";";

    public static List<Solid> createSolids(File file) {
        List<Solid> solids = new ArrayList<>();
        Scanner scanner = null;
        try {
            scanner = new Scanner(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        List<Face> faces = new ArrayList<>();
        String line = scanner.nextLine();
        while (scanner.hasNextLine()) {
            List<Point> points = new ArrayList<>();
            for (int i = 0; i < 3; i++) {
                line = scanner.nextLine();
                if (line.contains("SOLID")) {
                    line = scanner.nextLine();
                    solids.add(new Solid(faces, faces.size()));
                    faces = new ArrayList<>();
                }
                double x = Double.parseDouble(line.substring(0, line.indexOf(SREDNIK)));
                line = line.substring(line.indexOf(SREDNIK) + 1);
                double y = Double.parseDouble(line.substring(0, line.indexOf(SREDNIK)));
                double z = Double.parseDouble(line.substring(line.indexOf(SREDNIK) + 1));
                points.add(new Point(x, y, z));
            }
            faces.add(new Face(points.get(0), points.get(1), points.get(2)));
            scanner.nextLine();
        }
        solids.add(new Solid(faces, faces.size()));
        return solids;
    }

}
