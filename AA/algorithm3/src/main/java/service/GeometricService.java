package service;

import model.*;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import static java.lang.Math.abs;
import static java.lang.Math.sqrt;

public class GeometricService {

    /**
     * Right angle value
     */
    private static int RIGHT_ANGLE = 90;

    /**
     * Calculate distance between two points
     *
     * @param point0 {@link Point}
     * @param point1 {@link Point}
     * @return distance between them
     */
    public double dist(Point point0, Point point1) {
        return sqrt(Math.pow(point0.getX() - point1.getX(), 2) + Math.pow(point0.getY() - point1.getY(), 2) + Math.pow(point0.getZ() - point1.getZ(), 2));
    }

    /**
     * Returns distance between point and edge.
     *
     * @param point {@link Point}
     * @param edge  {@link Edge}
     * @return the shortest distance between them
     */
    public double dist(Point point, Edge edge) {
        double angle0 = 0;
        double angle1 = 0;
        try {
            angle0 = angle(new Edge(edge.getV1(), point), edge);
            angle1 = angle(new Edge(edge.getV2(), point), new Edge(edge.getV2(), edge.getV1()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (angle0 > RIGHT_ANGLE) {
            return dist(point, edge.getV1());
        } else if (angle1 > RIGHT_ANGLE) {
            return dist(point, edge.getV2());
        }
        return calculateHeight(point, edge);
    }

    /**
     * Calculate height in triangle based on point and the edge
     *
     * @param point {@link Point}
     * @param edge  {@link Edge}
     * @return value of height
     */
    private double calculateHeight(Point point, Edge edge) {
        double a = dist(edge.getV1(), edge.getV2());
        double b = dist(point, edge.getV1());
        double c = dist(point, edge.getV2());
        double p = 0.5 * (a + b + c);
        double field = sqrt(p * (p - a) * (p - b) * (p - c));
        double h = 2 * field / a;
        return h;
    }

    /**
     * Calculates angle between two edges. The edges must contains one (prefer first shared point)
     *
     * @param edge0
     * @param edge1
     * @return
     */
    public double angle(Edge edge0, Edge edge1) throws Exception {
        Point a;
        Point b;
        if (edge0.getV1().equals(edge1.getV1())) {
            a = edge0.getV2().getDistance(edge0.getV1());
            b = edge1.getV2().getDistance(edge1.getV1());
        } else if (edge0.getV1().equals(edge1.getV2())) {
            a = edge0.getV2().getDistance(edge0.getV1());
            b = edge1.getV2().getDistance(edge1.getV2());
        } else {
            throw new Exception("Incorrect input data");
        }
        Point shared = new Point(0, 0, 0);
        double scalar = a.getX() * b.getX() + a.getY() * b.getY() + a.getZ() * b.getZ();
        double distA = dist(a, shared);
        double distB = dist(b, shared);
        double cos = scalar / (distA * distB);
        double angle = Math.acos(cos);
        return Math.toDegrees(angle);
    }

    /**
     * Returns distance between two edges
     *
     * @param edge0 by {@link Edge}
     * @param edge1 by {@link Edge}
     * @return distance between them
     */
    public double dist(Edge edge0, Edge edge1) {
        if (intersect(edge0, edge1)) {
            return 0;
        }
        List<Double> distancesBetweenEdgesEnds = new ArrayList<>();
        distancesBetweenEdgesEnds.add(dist(edge0.getV1(), edge1));
        distancesBetweenEdgesEnds.add(dist(edge0.getV2(), edge1));
        distancesBetweenEdgesEnds.add(dist(edge1.getV1(), edge0));
        distancesBetweenEdgesEnds.add(dist(edge1.getV2(), edge0));
        return distancesBetweenEdgesEnds.stream().min(Comparator.naturalOrder()).get();
    }

    private boolean intersect(Edge edge0, Edge edge1) {
        boolean xy = intersect2D(edge0.getV1().getX(), edge0.getV1().getY(),
                edge0.getV2().getX(), edge0.getV2().getY(),
                edge1.getV1().getX(), edge1.getV1().getY(),
                edge1.getV2().getX(), edge1.getV2().getY());

        boolean xz = intersect2D(edge0.getV1().getX(), edge0.getV1().getZ(),
                edge0.getV2().getX(), edge0.getV2().getZ(),
                edge1.getV1().getX(), edge1.getV1().getZ(),
                edge1.getV2().getX(), edge1.getV2().getZ());

        boolean yz = intersect2D(edge0.getV1().getY(), edge0.getV1().getZ(),
                edge0.getV2().getY(), edge0.getV2().getZ(),
                edge1.getV1().getY(), edge1.getV1().getZ(),
                edge1.getV2().getY(), edge1.getV2().getZ());

        return (xy && xz && yz);
    }

    private boolean intersect2D(double x0, double y0, double x1, double y1, double x2, double y2, double x3, double y3) {
        double det = (x1 - x0) * (y3 - y2) - (x3 - x2) * (y1 - y0);
        if (det == 0) {
            return false;
        }
        double lambda = ((y3 - y2) * (x3 - x0) + (x2 - x3) * (y3 - y0)) / det;
        double gamma = ((y0 - y1) * (x3 - x0) + (x1 - x0) * (y3 - y0)) / det;
        return (0 < lambda && lambda < 1) && (0 < gamma && gamma < 1);
    }

    public double dist(Edge edge, Face face) {
        if (intersect(edge, face)) {
            return 0;
        }
        List<Double> distances = new ArrayList<>();
        List<Edge> edges = face.getEdges();
        for (int i = 0; i < edges.size(); i++) {
            distances.add(dist(edge, edges.get(i)));
        }
        distances.add(dist(face, edge.getV1()));
        distances.add(dist(face, edge.getV2()));

        return distances.stream().min(Comparator.naturalOrder()).get();
    }

    private boolean intersect(Edge edge, Face face) {
        return false;
    }

    public double dist(Face face, Point point) {
        Vector vectorV3toV2 = face.getV3().vectorToOtherPoint(face.getV2());
        Vector vectorV3toV1 = face.getV3().vectorToOtherPoint(face.getV1());
        Vector normal = vectorV3toV2.vectorMultiply(vectorV3toV1);
        double perpendicular = abs(point.vectorToOtherPoint(face.getV3()).scalarMultiply(normal) / normal.length());
        List<Edge> edges = face.getEdges();
        List<Double> distances = new ArrayList<>();
        for (int i = 0; i < edges.size(); i++) {
            distances.add(dist(point, edges.get(i)));
        }
        if (perpendicular == 0) {
            if (sharePlane(face, point)) {
                return 0;
            }
        } else {
            Point pointPlaneProjection = projectionToPlane(face, point);
            if(isPointOnFace(face, pointPlaneProjection)){
                distances.add(perpendicular);
            }
        }
        return distances.stream().min(Comparator.naturalOrder()).get();

    }

    private Point projectionToPlane(Face face, Point point) {
        Vector vectorV3toV2 = face.getV3().vectorToOtherPoint(face.getV2());
        Vector vectorV3toV1 = face.getV3().vectorToOtherPoint(face.getV1());
        Vector normal = vectorV3toV2.vectorMultiply(vectorV3toV1);
        double up = normal.scalarMultiply(face.getV3().toVectorFromZeroZeroZero()) - normal.scalarMultiply(point.toVectorFromZeroZeroZero());
        double down = Math.pow(normal.length(), 2);
        double t = up / down;
        return point.add(normal.multiply(t));
    }

    private boolean isPointOnFace(Face face, Point point) {
        double a = new Face(face.getV1(), face.getV2(), point).calculateArea();
        double b = new Face(face.getV1(), face.getV3(), point).calculateArea();
        double c = new Face(face.getV2(), face.getV3(), point).calculateArea();
        return calculateArea(face) == a + b + c;
    }

    private double calculateArea(Face face) {
        return face.normalToFace().length() / 2;
    }

    private boolean sharePlane(Face face, Point point) {
        boolean planeXY = isPointInsideTriangle(face.getV1().getX(), face.getV1().getY(),
                face.getV2().getX(), face.getV2().getY(),
                face.getV3().getX(), face.getV3().getY(),
                point.getX(), point.getY());

        boolean planeXZ = isPointInsideTriangle(face.getV1().getX(), face.getV1().getZ(),
                face.getV2().getX(), face.getV2().getZ(),
                face.getV3().getX(), face.getV3().getZ(),
                point.getX(), point.getZ());

        boolean planeYZ = isPointInsideTriangle(face.getV1().getY(), face.getV1().getZ(),
                face.getV2().getY(), face.getV2().getZ(),
                face.getV3().getY(), face.getV3().getZ(),
                point.getY(), point.getZ());

        return (planeXY & planeXZ & planeYZ);

    }

    private boolean isPointInsideTriangle(double ax, double ay, double bx, double by, double cx, double cy, double px, double py) {
        double d1 = sign(px, py, ax, ay, bx, by);
        double d2 = sign(px, py, bx, by, cx, cy);
        double d3 = sign(px, py, cx, cy, ax, ay);

        boolean has_neg = (d1 < 0) || (d2 < 0) || (d3 < 0);
        boolean has_pos = (d1 > 0) || (d2 > 0) || (d3 > 0);

        return !(has_neg && has_pos);
    }

    double sign(double ax, double ay, double bx, double by, double cx, double cy) {
        return (ax - cx) * (by - by) - (bx - cx) * (ay - cy);
    }

    public double dist(Face face0, Face face1) {
        List<Double> distances = new ArrayList<>();
        List<Edge> face0Edges = face0.getEdges();
        List<Edge> face1Edges = face1.getEdges();

        for (int i = 0; i < face0Edges.size(); i++) {
            distances.add(dist(face0Edges.get(i), face1));
            distances.add(dist(face1Edges.get(i), face0));
        }
        return distances.stream().min(Comparator.naturalOrder()).get();
    }

    public double dist(Solid solid0, Solid solid1) {
        List<Face> faces0 = solid0.getFaces();
        List<Face> faces1 = solid1.getFaces();
        List<Double> distances = new ArrayList<>();
        for (int i = 0; i < faces0.size(); i++) {
            for (int j = 0; j < faces1.size(); j++) {
                distances.add(dist(faces0.get(i), faces1.get(j)));
            }
        }
        return distances.stream().min(Comparator.naturalOrder()).get();
    }
}
