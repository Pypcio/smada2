import model.Matrix;
import org.junit.Assert;
import org.junit.Test;
import service.MatrixReader;

import java.io.File;
import java.util.List;

/**
 * Tests for matrix reader service
 */
public class MatrixReaderServiceTest {

    /**
     * Checks if correctly load matrix
     */
    @Test
    public void shouldLoadMatrixes() {
        // given
        ClassLoader classLoader = MatrixReaderServiceTest.class.getClassLoader();
        File file = new File(classLoader.getResource("matrix.txt").getFile());
        // when
        List<Matrix> matrices = MatrixReader.loadMatrixes(file);
        Matrix matrix = matrices.get(2);

        //then
        Assert.assertEquals(3, matrices.size());
        Assert.assertEquals(4, matrix.getCols());
        Assert.assertEquals(1, matrix.getRows());
    }
}
