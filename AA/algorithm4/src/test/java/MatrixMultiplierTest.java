import model.Matrix;
import org.junit.Assert;
import org.junit.Test;
import service.MatrixMultiplier;
import service.MatrixReader;
import service.MultiplierTask;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;

/**
 * Tests for matrix multiplier service
 */
public class MatrixMultiplierTest {

    /**
     * Checks if correctly multiply matrix
     */
    @Test
    public void shouldMultiplyMatrix() {
        ClassLoader classLoader = MatrixReaderServiceTest.class.getClassLoader();
        File file = new File(classLoader.getResource("multiply.txt").getFile());

        List<Matrix> matrices = MatrixReader.loadMatrixes(file);

        Matrix a = matrices.get(0);
        Matrix b = matrices.get(1);
        Matrix c = matrices.get(2);

        Matrix calculated = null;
        try {
            calculated = a.multiply(b);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Assert.assertTrue(calculated.equals(c));
    }

    /**
     * Checks if correctly multiply matrix list
     */
    @Test
    public void shouldMultiplyMatrixList() throws Exception {
        ClassLoader classLoader = MatrixReaderServiceTest.class.getClassLoader();
        File file = new File(classLoader.getResource("matrix_list.txt").getFile());
        List<Matrix> matrices = MatrixReader.loadMatrixes(file);

        Matrix a = matrices.get(0);
        Matrix b = matrices.get(1);
        Matrix c = matrices.get(2);
        Matrix d = matrices.get(3);

        Matrix resultOne = MatrixMultiplier.multiply(matrices);
        Matrix resultTwo1 = a.multiply(b);
        Matrix resultTwo2 = resultTwo1.multiply(c);
        Matrix resultTwo3 = resultTwo2.multiply(d);

        double[] aa = {-36, 29};
        double[] bb = {-45, 34};
        double[][] ab = {aa, bb};
        Matrix shouldBe = new Matrix(ab);
        Assert.assertTrue(resultOne.equals(resultTwo3));
        Assert.assertTrue(resultOne.equals(shouldBe));
    }

    /**
     * Checks if correctly multiply matrix by 2 thread
     */
    @Test
    public void shouldCalculateMultithreading() throws Exception {
        ClassLoader classLoader = MatrixReaderServiceTest.class.getClassLoader();
        File file = new File(classLoader.getResource("matrix_list.txt").getFile());
        List<Matrix> matrices = MatrixReader.loadMatrixes(file);
        Matrix result = MatrixMultiplier.multiply(matrices);


        ExecutorService executor = Executors.newFixedThreadPool(2);
        List<FutureTask> tasks = new ArrayList<>();
        FutureTask<Matrix> future = new FutureTask<>(new MultiplierTask(matrices.subList(0, 2)));
        FutureTask<Matrix> future2 = new FutureTask<>(new MultiplierTask(matrices.subList(2, 4)));
        tasks.add(future);
        tasks.add(future2);

        tasks.forEach(executor::execute);
        boolean isDone = false;

        while (!isDone) {
            Thread.sleep(100);
            if (tasks.get(0).isDone() && tasks.get(1).isDone()) {
                isDone = true;
            }
        }

        Matrix calculated0 = (Matrix) tasks.get(0).get();
        Matrix calculated1 = (Matrix) tasks.get(1).get();

        Matrix calculatedResult = calculated0.multiply(calculated1);

        Assert.assertTrue(calculatedResult.equals(result));
    }
}
