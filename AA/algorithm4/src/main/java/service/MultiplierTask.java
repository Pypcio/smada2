package service;

import model.Matrix;

import java.util.List;
import java.util.concurrent.Callable;

/**
 * Task to calculate Matrix
 */
public class MultiplierTask implements Callable<Matrix> {

    /**
     * {@link Matrix} list
     */
    List<Matrix> matrixList;

    /**
     * Creates instance of multiplier task
     * @param matrixList matrixes to calculate multiplier
     */
    public MultiplierTask(List<Matrix> matrixList) {
        this.matrixList = matrixList;
    }

    @Override
    public Matrix call(){
        return MatrixMultiplier.multiply(matrixList);
    }
}
