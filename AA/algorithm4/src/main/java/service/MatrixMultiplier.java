package service;

import model.Matrix;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;

public class MatrixMultiplier {

    /**
     * Multiply list of {@link Matrix}
     * @param matrixList list of matrix
     * @return result of multiplier
     * @throws Exception ir array not correct
     */
    public static Matrix multiply(List<Matrix> matrixList){
        if (matrixList.size() == 1) {
            return matrixList.get(0);
        }
        Matrix matrix = matrixList.get(0);
        for (int i = 1; i < matrixList.size(); i++) {
            matrix = matrix.multiply(matrixList.get(i));
        }
        return matrix;
    }

    /**
     * Gets submatrix from each task
     * @param tasks list of {@link FutureTask}
     * @return list of matrix
     * @throws InterruptedException
     * @throws java.util.concurrent.ExecutionException
     */
    private static List<Matrix> getMatricesFromFuture(List<FutureTask> tasks) throws InterruptedException, java.util.concurrent.ExecutionException {
        List<Matrix> calculatedMatrix = new ArrayList<>();
        for (int i = 0; i < tasks.size(); i++) {
            calculatedMatrix.add((Matrix) tasks.get(i).get());
        }
        return calculatedMatrix;
    }

    /**
     * Creates list of {@link FutureTask}
     * @param matrixList list of matrix
     * @param matrixPerThread matrix per single task/thread
     * @param threadNumber amount of thread used
     * @param lastThreadMatrixes size of last matrix
     * @return list of future tasks {@link FutureTask}
     */
    private static List<FutureTask> createFutureTasks(List<Matrix> matrixList, int matrixPerThread, int threadNumber, int lastThreadMatrixes) {
        List<FutureTask> tasks = new ArrayList<>();
        int i = 0;
        for (; i < threadNumber - 1; i++) {
            FutureTask<Matrix> future = new FutureTask<>(new MultiplierTask(matrixList.subList(i * matrixPerThread, (i + 1) * matrixPerThread)));
            tasks.add(future);
        }
        FutureTask<Matrix> future;
        if ((matrixList.size() - lastThreadMatrixes) - (matrixList.size() - 1) == 0) {
            future = new FutureTask<>(new MultiplierTask(Arrays.asList(matrixList.get(matrixList.size()))));
        } else {
            future = new FutureTask<>(new MultiplierTask(matrixList.subList(matrixList.size() - lastThreadMatrixes, matrixList.size())));
        }
        tasks.add(future);
        return tasks;
    }

    @Deprecated
    /**
     * Calculated result of multiplier of sublist of array list
     */
    public static Matrix multiplyRange(List<Matrix> matrixes, int begin, int end) {
        Matrix matrix = matrixes.get(begin);
        for (int i = begin + 1; i < end; i++) {
            try {
                matrix = matrix.multiply(matrixes.get(i));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return matrix;
    }


    /**
     * Multiply list of {@link Matrix} multithreading
     * @param matrixList list of matrix
     * @param matrixPerThread amount of matrix to task
     * @return result of multiplier
     * @throws Exception ir array not correct
     */
    public static Matrix multiply(List<Matrix> matrixList, int matrixPerThread) throws Exception {
        int threadNumber = BigDecimal.valueOf(matrixList.size() / matrixPerThread).setScale(0, BigDecimal.ROUND_FLOOR).toBigInteger().intValue();
        int lastThreadMatrixes = matrixList.size() - threadNumber * matrixPerThread;
        if (lastThreadMatrixes > 0) {
            threadNumber++;
        } else {
            return null;
        }
        ExecutorService executor = Executors.newFixedThreadPool(threadNumber);
        List<FutureTask> tasks = createFutureTasks(matrixList, matrixPerThread, threadNumber, lastThreadMatrixes);

        Instant start = Instant.now();

        for (int i = 0; i < tasks.size(); i++) {
            executor.execute(tasks.get(i));
        }

        boolean isDone = false;

        while (!isDone) {
            int finished = 0;
            for (int i = 0; i < tasks.size(); i++) {
                if (tasks.get(i).isDone()) {
                    finished++;
                }
            }
            if (finished == tasks.size()) {
                isDone = true;
            } else {
                Thread.sleep(0,100);
            }
        }

        List<Matrix> calculatedMatrix = getMatricesFromFuture(tasks);
        Matrix result = MatrixMultiplier.multiply(calculatedMatrix);
        Instant elapsedTimeMultithreading = Instant.now();
        System.out.println("total time multithreading: " + Duration.between(start, elapsedTimeMultithreading).toMillis());
        executor.shutdown();
        return result;
    }
}
