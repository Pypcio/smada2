package service;

import model.Matrix;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static java.lang.Math.pow;

/**
 * Reader for given matrix (with a little adjust)
 */
public class MatrixReader {

    /**
     * final statics Strings use in the service
     */
    private static final String M_CHAR = "M";
    private static final String SMALL_E_CHAR = "e";
    private static final String SPACE = " ";
    private static final String EMPTY_STRING = "";

    /**
     * Loads list of {@link Matrix} from given {@link File}
     * @param file to the source of matrixes
     * @return List of matrixes
     */
    public static List<Matrix> loadMatrixes(File file) {

        Scanner scanner = null;
        try {
            scanner = new Scanner(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        List<Matrix> matrices = new ArrayList<Matrix>();
        double[][] matrix = new double[0][0];
        double[] array = new double[0];
        int m = -1;
        int n = 0;
        int cols = 0;
        int rows;
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            if (line.contains(M_CHAR)) {
                rows = Integer.parseInt(line.substring(line.indexOf(SPACE) + 1, line.lastIndexOf(SPACE)));
                cols = Integer.parseInt(line.substring(line.lastIndexOf(SPACE) + 1));
                m++;
                if (m > 0) {
                    matrices.add(new Matrix(matrix));
                }
                matrix = new double[rows][cols];
                array = new double[cols];
                n = 0;
                line = scanner.nextLine();
            }
            int i = 0;
            while (!line.equals(EMPTY_STRING)) {
                double first;
                String valueByString;
                if (line.contains(SPACE)) {
                    valueByString = line.substring(0, line.indexOf(SPACE));
                } else {
                    valueByString = line;
                }
                if (valueByString.contains(SMALL_E_CHAR)){
                    String value = valueByString.substring(0, valueByString.indexOf(SMALL_E_CHAR));
                    int factor = Integer.parseInt(valueByString.substring(valueByString.length()-3));
                    first = Double.valueOf(value) * pow(10, factor);
                } else {
                    first = Double.valueOf(valueByString);
                }
                array[i] = first;
                i++;
                if (line.contains(SPACE)) {
                    line = line.substring(line.indexOf(SPACE) + 1);
                } else {
                    line = EMPTY_STRING;
                    matrix[n] = array;
                    array = new double[cols];
                    n++;
                }
            }
        }
        matrices.add(new Matrix(matrix));
        return matrices;
    }

}
