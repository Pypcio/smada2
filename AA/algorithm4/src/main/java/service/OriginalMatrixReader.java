package service;

import model.Matrix;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class OriginalMatrixReader {
    /**
     * Loads list of {@link Matrix} from given {@link File}
     *
     * @param file to the source of matrixes
     * @return List of matrixes
     */
    public static List<Matrix> loadMatrixes(File file) {

        Scanner scanner = null;
        try {
            scanner = new Scanner(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        List<Matrix> matrices = new ArrayList<Matrix>();
        StringBuilder sb = new StringBuilder();
        int rowsNo = 0, colsNo = 0;
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            if (line.contains("Matrix")) {
                sb = new StringBuilder();
                Pattern pattern = Pattern.compile("(\\d)\\s+x\\s+(\\d)");
                Matcher matcher = pattern.matcher(line);
                matcher.find();
                rowsNo = Integer.valueOf(matcher.group(1));
                colsNo = Integer.valueOf(matcher.group(2));
            } else if (line.contains("---------------")) {
                matrices.add(parseStringToMatrix(sb.toString(), rowsNo, colsNo));
            } else {
                sb.append(line);
            }
        }
        return matrices;
    }


    private static Matrix parseStringToMatrix(String string, int rowsNo, int colsNo) {
        Scanner scanner = new Scanner(string);
        double[][] array = new double[rowsNo][colsNo];
        for (int i = 0; i < rowsNo; i++) {
            double[] values = new double[colsNo];
            for (int j = 0; j < colsNo; j++) {
                String next = scanner.next();
                String number = next.replaceAll("[\\[,\\]]", "");
                values[j] = Double.valueOf(number);
            }
                array[i] = values;
        }
        return new Matrix(array);
    }
}
