package model;

/**
 * Matrix implementation
 */
public class Matrix {

    /**
     * 2d array of matrix
     */
    private double array[][];

    /**
     * rows amount
     */
    private int rows;

    /**
     * cols amount
     */
    private int cols;

    /**
     * Creates instance of Matrix
     *
     * @param array 2d array
     */
    public Matrix(double[][] array) {
        this.array = array;
        rows = array.length;
        cols = array[0].length;
    }

    /**
     * Gets array
     * @return array
     */
    public double[][] getArray() {
        return array;
    }

    /**
     * Gets rows amount
     * @return rows
     */
    public int getRows() {
        return rows;
    }

    /**
     * Get cols amount
     * @return cols
     */
    public int getCols() {
        return cols;
    }

    /**
     * Multiply matrix with other
     * @param other {@link Matrix}
     * @return new Matrix
     * @throws Exception when sizes not correct
     */
    public Matrix multiply(Matrix other) {
        Matrix newMatrix;
        if (cols == other.getRows()) {
            double[][] newMatrixArray = new  double[rows][other.getCols()];
            newMatrix = new Matrix(newMatrixArray);
            for (int i = 0; i < rows ; i++){
                // i == row in original matrix
                double[] newRow = new double[other.getCols()];
                for (int j = 0; j< other.getCols() ; j++){
                    // j == calculated col in new matrix
                    double newValue = 0;
                    for (int k = 0; k< cols; k++){
                        // k == col in original matrix
                        newValue += array[i][k] * other.getArray()[k][j];
                    }
                    newRow[j] = newValue;
                }
                newMatrixArray[i] = newRow;
            }

        } else {
            throw new RuntimeException();
        }
        return newMatrix;
    }

    /**
     * Checks if matrixes are equal
     * @param other {@link Matrix} to check
     * @return true if that same
     */
    public boolean equals(Matrix other) {
        if (other.getCols() == cols && other.getRows() == rows) {
            for (int i = 0; i < rows; i++) {
                for (int j = 0; j < cols; j++) {
                    if (array[i][j] != other.getArray()[i][j]) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    /**
     * Cast matrix to string
     * @return
     */
    public String print(){
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i< rows; i++){
            for (int j = 0; j< cols; j++){
                stringBuilder.append(array[i][j]).append(" ");
            }
            stringBuilder.append("\n");
        }
        return stringBuilder.toString();
    }
}
