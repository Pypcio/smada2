import model.Matrix;
import service.MatrixMultiplier;
import service.OriginalMatrixReader;

import java.io.File;
import java.time.Duration;
import java.time.Instant;
import java.util.List;

public class Main {

    /**
     * Matrix per task to multiplier
     */
    private final static int MATRIX_PER_TASK = 300;

    /**
     * File to load data
     */
    private final static String FILE_NAME = "data2.txt";


    public static void main(String[] args) throws Exception {


        ClassLoader classLoader = Main.class.getClassLoader();
        File file = new File(classLoader.getResource(FILE_NAME).getFile());

        boolean multithreading = true;
        boolean singlethreading = true;


        List<Matrix> matrixList = OriginalMatrixReader.loadMatrixes(file);
        if (multithreading) {
            Matrix finalMatrixWithMultithreading = MatrixMultiplier.multiply(matrixList, MATRIX_PER_TASK);
            System.out.println(finalMatrixWithMultithreading.print());
        }

        if (singlethreading) {
            Instant start = Instant.now();
            Matrix finalMatrixWithoutThreads = MatrixMultiplier.multiply(matrixList);
            Instant elapsedTime = Instant.now();
            System.out.println("total time single thread: " + Duration.between(start, elapsedTime).toMillis() + " milis");
            System.out.println(finalMatrixWithoutThreads.print());
        }
    }
}
