package services;

import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.WeightedPseudograph;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class GraphService {

    private static final String DISTIBUTOR = "\t";


    private static final int BREAK_VALUES = 1;

    public static Graph<Integer, DefaultWeightedEdge> createGraph(File file) {
        Graph<Integer, DefaultWeightedEdge> graph = new WeightedPseudograph<>(DefaultWeightedEdge.class);
        Scanner scanner = null;
        try {
            scanner = new Scanner(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Set<Integer> nodes = new HashSet<>();
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            nodes.add(Integer.valueOf(line.substring(0, line.indexOf(DISTIBUTOR))));
            nodes.add(Integer.valueOf(line.substring(line.indexOf(DISTIBUTOR) + BREAK_VALUES, line.lastIndexOf(DISTIBUTOR))));
        }
        for (Integer node : nodes) {
            graph.addVertex(node);
        }
        return graph;
    }

    public static void fillGraph(Graph<Integer, DefaultWeightedEdge> graph, File file) {
        Scanner scanner = null;
        try {
            scanner = new Scanner(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            Integer source = Integer.valueOf(line.substring(0, line.indexOf(DISTIBUTOR)));
            Integer destination = Integer.valueOf(line.substring(line.indexOf(DISTIBUTOR) + BREAK_VALUES, line.lastIndexOf(DISTIBUTOR)));
            Double weight = Double.valueOf(line.substring(line.lastIndexOf(DISTIBUTOR) + BREAK_VALUES));
            DefaultWeightedEdge edge = graph.addEdge(source, destination);
            graph.setEdgeWeight(edge, weight);
        }
    }

    public Graph<Integer, DefaultWeightedEdge> copyGraph(Graph<Integer, DefaultWeightedEdge> orginal){
        Graph<Integer, DefaultWeightedEdge> newGraph = new WeightedPseudograph<>(DefaultWeightedEdge.class);
        Set<Integer> vertexes = orginal.vertexSet();
        for (int vertex : vertexes) {
            newGraph.addVertex(new Integer(vertex));
        }
        Set<DefaultWeightedEdge> edges = orginal.edgeSet();
        for (DefaultWeightedEdge edge: edges) {
            Integer source = orginal.getEdgeSource(edge);
            Integer destination = orginal.getEdgeTarget(edge);
            Double weight = orginal.getEdgeWeight(edge);
            DefaultWeightedEdge newEdge = newGraph.addEdge(source, destination);
            newGraph.setEdgeWeight(newEdge, weight);
        }
        return newGraph;
    }
}
