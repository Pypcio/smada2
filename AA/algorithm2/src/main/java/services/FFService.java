package services;

import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultWeightedEdge;

import java.util.*;

public class FFService {

    private Graph<Integer, DefaultWeightedEdge> graph;
    List<Integer> path;


    public FFService(Graph<Integer, DefaultWeightedEdge> graph, List<Integer> path) {
        this.graph = graph;
        this.path = path;
    }

    public double calculateFlow() {
        Map<Integer, Double> residualMap = calculateResidualMap();
        Optional<Double> residual = calculatePathFlow(residualMap);
//        System.out.println("single flow: " + residual);
        if (residual.isPresent()){
            reduceFlowOnPath(residual.get());
            return residual.get();
        }
        return 0;
    }

    private Map<Integer, Double> calculateResidualMap() {
        Map<Integer, Double> residualMap = new HashMap<>();
        for (int i = 0; i < path.size() - 1; i++) {
            Integer node = path.get(i);
            DefaultWeightedEdge edge = graph.getAllEdges(path.get(i), path.get(i + 1)).stream().filter(x ->node==graph.getEdgeSource(x)).findFirst().get();
            residualMap.put(path.get(i), graph.getEdgeWeight(edge));
        }
        return residualMap;
    }

    private void reduceFlowOnPath(double residual) {
        for (int i = 0; i < path.size() - 1; i++) {
            Integer node = path.get(i);
            DefaultWeightedEdge edge = graph.getAllEdges(path.get(i), path.get(i + 1)).stream().filter(x ->node==graph.getEdgeSource(x)).findFirst().get();
            Double newWeight = graph.getEdgeWeight(edge) - residual;
            graph.setEdgeWeight(edge, newWeight);
        }
    }

    private Optional<Double> calculatePathFlow(Map<Integer, Double> residualMap) {
        Optional<Double> flow = residualMap.values().stream().min(Comparator.naturalOrder());
        return flow;
    }

    public void setPath(List<Integer> path) {
        this.path = path;
    }
}
