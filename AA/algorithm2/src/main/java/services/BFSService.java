package services;

import com.google.common.collect.Lists;
import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultWeightedEdge;

import java.util.*;

public class BFSService {

    public List<Integer> findBFS(Integer source, Integer destination, Graph graph) {

        Set<Integer> nodeToSettle = new HashSet<>(graph.vertexSet());
        Map<Integer, Integer> settleMap = implementSettleMap(source, nodeToSettle);
        Map<Integer, Integer> previousVertexMap = implementPreviousVertexMap(source, nodeToSettle);

        int level = 0;
        while (!nodeToSettle.isEmpty()) {
            settleNeightbours(level, graph, nodeToSettle, settleMap, previousVertexMap);
            level++;
        }
        List<Integer> pathToDestination = countPath(source, destination, previousVertexMap);
        return pathToDestination;
    }


    private void settleNeightbours(Integer level, Graph graph, Set<Integer> nodeToSettle,
                                   Map<Integer, Integer> settleMap, Map<Integer, Integer> previousVertexMap) {
        List<Integer> currentNodes = getCurrentNodes(level, settleMap);
        for (Integer currentNode : currentNodes) {
            nodeToSettle.remove(currentNode);
            Set<DefaultWeightedEdge> edges = graph.outgoingEdgesOf(currentNode);
            List<Integer> neightbours = new ArrayList<>();
            List<DefaultWeightedEdge> edgesToRemove = new ArrayList<>();
            for (DefaultWeightedEdge edge : edges) {
                Integer source = (Integer) graph.getEdgeSource(edge);
                if (source == currentNode) {
                    if (graph.getEdgeWeight(edge) > 0) {
                        neightbours.add((Integer) graph.getEdgeTarget(edge));
                    } else {
                        edgesToRemove.add(edge);
                    }
                }
            }
            graph.removeAllEdges(edgesToRemove);
            for (int i : neightbours) {
                if (settleMap.get(i) > level) {
                    settleMap.put(i, level + 1);
                    previousVertexMap.put(i, currentNode);
                    nodeToSettle.remove(i);
                }
            }
        }
        if (currentNodes.isEmpty()){
            nodeToSettle.clear();
        }
    }

    private List<Integer> getCurrentNodes(Integer level, Map<Integer, Integer> settleMap) {
        List<Integer> nodes = new ArrayList<>();
        settleMap.entrySet().stream().filter(x -> x.getValue().equals(level)).forEach(n -> nodes.add(n.getKey()));
        return nodes;
    }

    private List<Integer> countPath(Integer source, Integer destination, Map<Integer, Integer> previousVertexMap) {
        List<Integer> reachable = new ArrayList<>();
        reachable.add(destination);
        Integer testedVertex = destination;
        Optional<Integer> vertex = Optional.of(destination);
        while (!reachable.contains(source) && vertex.isPresent()) {
            vertex = Optional.ofNullable(previousVertexMap.get(testedVertex));
            if (vertex.isPresent()){
                if (vertex.get() == Integer.MAX_VALUE){
                    // there is no value so return empty list
                    return new ArrayList<>();
                }
                reachable.add(vertex.get());
                testedVertex = vertex.get();
            }
        }
        return Lists.reverse(reachable);
    }

    private Map<Integer, Integer> implementPreviousVertexMap(Integer source, Set<Integer> nodeToSettle) {
        HashMap hashMap = new HashMap();
        for (Integer i : nodeToSettle) {
            hashMap.put(i, Integer.MAX_VALUE);
        }
        hashMap.put(source, 0);
        return hashMap;
    }

    private Map<Integer, Integer> implementSettleMap(Integer source, Set<Integer> vertexes) {
        HashMap hashMap = new HashMap();
        for (Integer i : vertexes) {
            hashMap.put(i, Integer.MAX_VALUE);
        }
        hashMap.put(source, 0);
        return hashMap;
    }

}
