import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultWeightedEdge;
import services.BFSService;
import services.FFService;
import services.GraphService;

import java.io.File;
import java.util.*;

public class Main {

    public static void main(String args[]) {

        ClassLoader classLoader = Main.class.getClassLoader();
        File file = new File(classLoader.getResource("nodes.txt").getFile());

        // ex. 2:
        Graph<Integer, DefaultWeightedEdge> graph = GraphService.createGraph(file);
        GraphService.fillGraph(graph, file);
        Double exe2 = calculateMaximumFlowBetween(10,60,graph);
        System.out.println("Result of ex.2: " + exe2);

        // there are few targets with very near value to the maximum.
        int exe3 = exercise_3(10, file);
        System.out.println("Result of ex.3: " + exe3);
    }

    public static Double calculateMaximumFlowBetween(int source, int destination, Graph<Integer, DefaultWeightedEdge> graph){
        BFSService bfsService = new BFSService();
        double maximumFlow = 0;
        List<Integer> path = bfsService.findBFS(source, destination, graph);
        FFService ffService = new FFService(graph, path);
        Double flow;
        do {
//            System.out.println(path);
            ffService.setPath(path);
            flow = ffService.calculateFlow();
            maximumFlow += flow;
            path = bfsService.findBFS(source, destination, graph);
        } while (!path.isEmpty() && flow > 0);
//        System.out.println("total maximum flow: " + maximumFlow);
        return maximumFlow;
    }

    public static int exercise_3(int source, File file) {
        Graph<Integer, DefaultWeightedEdge> graph = GraphService.createGraph(file);
        Map<Integer, Double> maximumFlowMap = new HashMap<>();
        int lastGraphVertex = graph.vertexSet().stream().max(Comparator.naturalOrder()).get();
        for (int i = 0; i<lastGraphVertex; i++){
            Graph tempGraph = GraphService.createGraph(file);
            GraphService.fillGraph(tempGraph, file);
            if (i != source){
                double flow = calculateMaximumFlowBetween(source, i, tempGraph);
                System.out.println(i + ": " + flow);
                maximumFlowMap.put(i, flow);
            }
        }
        int highestFlowVertex = maximumFlowMap.entrySet().stream().max((entry1, entry2) -> entry1.getValue() > entry2.getValue() ? 1 : -1).get().getKey();
        Double value = maximumFlowMap.get(highestFlowVertex);
        System.out.println("Maximum flow is on: " + highestFlowVertex + " and contains: " + value);
        return highestFlowVertex;
    }

}
